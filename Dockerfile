FROM portailt7s/rhel72


LABEL io.k8s.description="Dockerfile PortailSOA StefIT" \
      io.k8s.display-name="Portail Tomcat custom StefIT" \
      io.openshift.expose-services="8501:http" \
      io.openshift.tags="Tomcat,java"

#ENV	WARSRCHTTP http://nexus.stef.com/nexus/content/repositories/snapshots/com/stef/soa/portail/1.55-SNAPSHOT/portail-1.55-20161209.161510-587.war
RUN 	curl ${WARSRCHTTP} -o /opt/tomcat/webapps/${APPSTEF}.war

#ENV APPSTEF portail
RUN mkdir -p /opt/${APPSTEF}/conf \
	mkdir /opt/${APPSTEF}/log \
	mkdir /opt/${APPSTEF}/data


WORKDIR /opt/tomcat

EXPOSE 8501

CMD ["catalina.sh","run"]

